import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ITienda } from 'src/app/models/tienda.model';

@Component({
  selector: 'card-tienda',
  templateUrl: './card-tienda.component.html',
  //styleUrls: ['./card-tiendas.component.scss']
})
export class CardTiendaComponent implements OnInit {

    @Input() tienda: ITienda
    @Output() onSelected: EventEmitter<ITienda> = new EventEmitter();

    constructor() { }

    ngOnInit() {

    }

    actionSelected(){
      this.onSelected.emit(this.tienda)
    }

}
