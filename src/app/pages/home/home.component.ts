import { Component, OnInit } from '@angular/core';
import { ISlideModel } from 'src/app/models/slide.model';
import { BannersService } from 'src/app/services/banners.service';
import { isArray } from 'util';
import { TiendasService } from 'src/app/services/tiendas.service';
import { ITienda } from 'src/app/models/tienda.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  slides: Array<ISlideModel>
  tiendas: Array<ITienda>

  constructor(
    private _router: Router,
    public bannerService: BannersService,
    public tiendasService: TiendasService
  ) { 
    this.tiendas = this.tiendasService.collection
  }

  ngOnInit() {
    this.initData()
  }

 initData(){
    this.bannerService.findAll().toPromise().then((response) => {
     
      this.slides = new Array<ISlideModel>()
      if(isArray(response.data)){
        response.data.forEach(slide => {
          this.slides.push({
            image: slide.image,
            title: slide.name,
            subtitle: slide.description
          })
        })
      }

    })
  }

  verDetalle(tienda: ITienda){
    this._router.navigate(['/tiendas/'+tienda.id])
  }

}
