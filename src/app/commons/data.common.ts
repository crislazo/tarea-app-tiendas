
export const TIENDAS = [
    {id: 1, nombre: "Totus Pachacutec", direccion: "Av Pachacutec 220", telefono: "(01) 2345675"},
    {id: 2, nombre: "Metro Angamos", direccion: "Av Pachacutec 890", telefono: "(01) 2345675"},
    {id: 3, nombre: "Electra VTM", direccion: "Av Pachacutec 520", telefono: "(01) 2345675"},
    {id: 4, nombre: "HomeCenter Angamos", direccion: "Av Pachacutec 890", telefono: "(01) 2345675"},
    {id: 5, nombre: "Saga Falabella Angamos", direccion: "Av Pachacutec 890", telefono: "(01) 2345675"}
]

export const HORARIOS = [
    {id: 1, tienda_id: 1, nombre: "lunes a sabado", start_time: "08:00:00", end_time: "10:00:00"},
    {id: 2, tienda_id: 1, nombre: "domingos", start_time: "08:00:00", end_time: "09:00:00"},
    {id: 3, tienda_id: 2, nombre: "lunes a domingo", start_time: "08:00:00", end_time: "10:00:00"},
    {id: 4, tienda_id: 3, nombre: "todos los días", start_time: "08:00:00", end_time: "10:00:00"},
    {id: 5, tienda_id: 4, nombre: "todos los días", start_time: "08:00:00", end_time: "10:00:00"},
    {id: 6, tienda_id: 5, nombre: "todos los días", start_time: "08:00:00", end_time: "10:00:00"}
]