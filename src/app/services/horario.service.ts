import {Injectable} from '@angular/core';
import { ITienda, IHorario } from '../models/tienda.model';
import { HORARIOS } from '../commons/data.common';

@Injectable({providedIn: 'root'})

export class HorarioService {

  public collection: Array<IHorario> = HORARIOS
  public item: IHorario

  constructor() {
    
  }


  delete(id: number){
    let index = this.collection.findIndex(v => v.id == id)
    if(index != -1){
        this.collection.splice(index, 1)
    }    
  }

  buscarPorTienda(tienda: ITienda): Array<IHorario>{
    return this.collection.filter(item => item.tienda_id == tienda.id)
  }

  get(id: number): IHorario{
      return this.collection.find(v => v.id == id)
  }

  add(horario: IHorario){
    if(!horario.id){
      let index = this.collection.length
      horario.id = index + 1
    }
    this.collection.push(horario)
  }

  update(horario: IHorario){
    let index = this.collection.findIndex(v => v.id == horario.id)
    if(index != -1){
        this.collection[index] = horario
    }
  }

}