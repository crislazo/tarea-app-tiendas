import { Component } from '@angular/core';
import { IndexDBService } from './services/indexdb.service';
import { setTheme } from 'ngx-bootstrap/utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mi-tienda';

  constructor(public idbService: IndexDBService){
    setTheme('bs3');
  }

  insertarDatos(){
    this.idbService.insert('tiendas', {name: 'marianas', email: 'topitop@gmail.com'})
  }
  eliminarTabla(){
    this.idbService.clear('tiendas')
  }
}
