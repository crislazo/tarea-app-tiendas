import { Component, OnInit } from '@angular/core';
import { ITienda } from 'src/app/models/tienda.model';
import { TiendasService } from 'src/app/services/tiendas.service';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.scss']
})
export class TiendasComponent implements OnInit {

  tiendas: Array<ITienda>
  searchTienda: string

  constructor(
    public tiendasService: TiendasService
  ) {

    this.tiendas = this.tiendasService.collection

  }

  ngOnInit() {
        
  }

  eliminar(tienda: ITienda){
    this.tiendasService.delete(tienda.id)
  }

  loadTiendas(){
    
  }

 


}
