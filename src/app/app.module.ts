import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRouteModule } from './app.route';
import PAGES_COMPONENTS from './pages';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import APP_COMPONENTS from './components';
import { HttpClientModule } from '@angular/common/http';
import { FilterdataPipe } from './pipes/filterdata.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PAGES_COMPONENTS,
    APP_COMPONENTS,
    FilterdataPipe
  ],
  imports: [
    BrowserModule,
    AppRouteModule,
    HttpClientModule,
    FormsModule,
    CarouselModule.forRoot(),
  ],
  exports:[
    APP_COMPONENTS
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
