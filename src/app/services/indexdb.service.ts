import {Injectable} from '@angular/core';
import { NgxIndexedDB } from 'ngx-indexed-db';

@Injectable({providedIn: 'root'})
export class IndexDBService {

  public db: NgxIndexedDB

  constructor() {
    this.db = new NgxIndexedDB('mitienda', 20);
  }


  openDatabase(nameModel:string){
    return this.db.openDatabase(1, evt => {
        let objectStore = evt.currentTarget.result.createObjectStore(nameModel, { keyPath: '_index_', autoIncrement: true });
        /*objectStore.createIndex('name', 'name', { unique: false });
        objectStore.createIndex('email', 'email', { unique: true });*/
    })
  }

  getByKey(nameModel, id){
      this.openDatabase(nameModel).then(result => {
        this.db.getByKey(nameModel, id).then(
            person => {
                console.log(person);
            },
            error => {
                console.log(error);
            }
        )
      })    
  }

  insert(nameModel, data){
    this.openDatabase(nameModel).then(result => {
    this.db.add(nameModel, data).then(
        person => {
            console.log(person);
        },
        error => {
            console.log(error);
        }
    )
    })
  }

  clear(table){
    this.db.clear(table).then(
        () => {
            // Do something after clear
        },
        error => {
            console.log(error);
        }
    );
  }

}