export const environment = {
  production: true,
  api:{
    BASE_URL: '/assets/json-data/',
    CLIENT_ID: 'CLIENT_ID'
  }
};
