import { HomeComponent } from "./home/home.component";
import { TiendaComponent } from "./tienda/tienda.component";
import { TiendasComponent } from "./tiendas/tiendas.component";

const PAGES_COMPONENTS = [
    HomeComponent,
    TiendaComponent,
    TiendasComponent
]
export default PAGES_COMPONENTS