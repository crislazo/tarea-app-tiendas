import {Injectable} from '@angular/core';
import { ITienda } from '../models/tienda.model';
import { TIENDAS } from '../commons/data.common';

@Injectable({providedIn: 'root'})

export class TiendasService {

  public collection: Array<ITienda> = TIENDAS
  public item: ITienda

  constructor() {
    
  }

  delete(id: number){
    let index = this.collection.findIndex(v => v.id == id)
    if(index != -1){
        this.collection.splice(index, 1)
    }    
  }

  get(id: number): ITienda{
      return this.collection.find(v => v.id == id)
  }

  add(tienda: ITienda): ITienda{
    if(!tienda.id){
      let index = this.collection.length
      tienda.id = index + 1
    }
    this.collection.push(tienda)
    return tienda
  }

  update(tienda: ITienda){
    let index = this.collection.findIndex(v => v.id == tienda.id)
    if(index != -1){
        this.collection[index] = tienda
    }
  }


}