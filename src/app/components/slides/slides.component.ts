import { Component, OnInit, Input } from '@angular/core';
import { ISlideModel } from 'src/app/models/slide.model';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss']
})
export class SlidesComponent implements OnInit {

  @Input() slides: Array<ISlideModel>

  constructor() { }

  ngOnInit() {
    
  }

}
