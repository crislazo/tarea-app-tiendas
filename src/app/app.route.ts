import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { TiendasComponent } from './pages/tiendas/tiendas.component';
import { TiendaComponent } from './pages/tienda/tienda.component';

const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent},
    { path: 'home', component: HomeComponent},
    { path: 'tiendas', component: TiendasComponent},
    { path: 'tiendas/new', component: TiendaComponent},
    { path: 'tiendas/:id', component: TiendaComponent},
    { path: '**', redirectTo: '/404'}
    //{ path: '', redirectTo: 'Login', pathMatch: 'full' }
    // { path: 'Seguridad/404', component: NotFoundComponent },
    // {path: '**', redirectTo: '/404'}
];

@NgModule({
    imports: [RouterModule.forRoot(APP_ROUTES, {})],
    exports: [RouterModule],
    providers: []
})

export class AppRouteModule {
    constructor(private router: Router) {
        this.router.errorHandler = (error: any) => {
            this.router.navigate(['NotFound']);
        };
      }
 }
