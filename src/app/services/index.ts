import { ApiService } from "./api.service";
import { IndexDBService } from "./indexdb.service";
import { BannersService } from "./banners.service";

const APP_SERVICES = {
    ApiService: ApiService,
    IndexDBService: IndexDBService,
    BannersService: BannersService
}
export default APP_SERVICES