export class ISlideModel{
    image: string
    title: string
    subtitle: string
}
