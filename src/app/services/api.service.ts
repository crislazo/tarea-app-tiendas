import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({providedIn: 'root'})
export class ApiService {

  public baseUrl: string

  constructor(private http: HttpClient) {
    this.baseUrl = environment.api.BASE_URL
  }

  private getToken(): string {
    const token = localStorage.getItem('TOKEN_SESSION');
    return token;
  }

  private getHeaders(): any {
    let headers = new HttpHeaders();
    const token = this.getToken();
    headers = headers.set('CLIENT_ID', environment.api.CLIENT_ID)
    if(token){
      headers = headers.set('Authorization', token)
    }
    headers = headers.set('Content-Type', 'application/json')
    return headers;
  }

  private getBaseUrl(uri: string){
    let baseUrl = this.baseUrl + uri
    return baseUrl
  }

  
  public get(uri:string, params?: any): Observable<any> {
    return this.http.get(this.getBaseUrl(uri), {headers: this.getHeaders(), params: params});
  }

  public post(uri:string, data?: any, params?: any): Observable<any> {
    return this.http.post(this.getBaseUrl(uri), data, {headers: this.getHeaders(), params: params});
  }

  public put(uri:string, data?: any, params?: any): Observable<any> {
    return this.http.put(this.getBaseUrl(uri), data, {headers: this.getHeaders(), params: params});
  }


}
