export interface ITienda{
    id?: number
    nombre: string
    direccion: string
    telefono: string
}

export interface IHorario{
    id?: number
    tienda_id?: number
    nombre: string
    start_time: string
    end_time: string
}
