import { Component, OnInit } from '@angular/core';
import { TiendasService } from 'src/app/services/tiendas.service';
import { IHorario, ITienda } from 'src/app/models/tienda.model';
import { HorarioService } from 'src/app/services/horario.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.scss']
})
export class TiendaComponent implements OnInit {

  disabledForm: boolean = false
  tienda_id: number
  tienda: ITienda = <ITienda>{}
  horarios: Array<IHorario> = new Array<IHorario>()

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    public tiendasService: TiendasService,
    public horarioService: HorarioService
  ) { }

  ngOnInit() {

    this.tienda_id = this._route.snapshot.paramMap.get('id')?parseInt(this._route.snapshot.paramMap.get('id')):0
    
    
    if(this.tienda_id){
      this.disabledForm = true
      this.tienda = this.tiendasService.get(this.tienda_id)
      if(!this.tienda){
        this.salir()
      }
      this.horarios = this.horarioService.buscarPorTienda(this.tienda)
    }
    
  }

  guardar(){

    if(this.tienda.id){
      this.tiendasService.update(this.tienda)
    }else{
      this.tienda = this.tiendasService.add(this.tienda)
    }
    
    this.horarios.forEach(item => {
      item.tienda_id = this.tienda.id
      if(item.nombre.length > 0 && item.start_time.length > 0 && item.end_time.length > 0){
        if(!item.id){
          this.horarioService.add(item)
        }else{
          this.horarioService.update(item)
        }
      }
    })

    this.salir()
  }

  addHorario(){
    
      this.horarios.push({
        nombre: "",
        start_time: "",
        end_time: ""
      })
         
  }

  salir(){
    this._router.navigate(['/tiendas'])
  }

}

