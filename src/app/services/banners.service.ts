import {Injectable} from '@angular/core';
import { ApiService } from './api.service';

@Injectable({providedIn: 'root'})

export class BannersService {

  constructor(public apiService: ApiService) {
      
  }

  findAll(params?:any){
    return this.apiService.get("banners.json", params)
  }

}