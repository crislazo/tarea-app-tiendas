import {NgModule} from '@angular/core';
import { MenuComponent } from './menu/menu.component';
import { SlidesComponent } from './slides/slides.component';
import { FooterComponent } from './footer/footer.component';
import { CardTiendaComponent } from './cards/card-tiendas/card-tienda.component';

const APP_COMPONENTS = [
    MenuComponent,
    SlidesComponent,
    FooterComponent,
    CardTiendaComponent
  ]

  export default APP_COMPONENTS